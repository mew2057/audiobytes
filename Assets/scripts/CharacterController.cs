﻿using UnityEngine;
using System.Collections;

public class CharacterController : MonoBehaviour {
	public float maxSpeed = 1f;
	public float minSpeed = 0f;

	public float jumpSpeed = 3f;

	public bool grounded = false;
	public Transform groundCheck;
	public float groundRadius;
	public LayerMask whatIsGround;

	public Animator anim;
	private bool didJump = false;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
	}

	// Update is called once per frame
	void Update () {
		didJump = didJump || Input.GetButtonDown (KeyCode.Space.ToString ());
	}

	void FixedUpdate()
	{
		grounded = Physics2D.OverlapCircle (groundCheck.position, groundRadius, whatIsGround;

		float move = Input.GetAxis("Horizontal");

		anim.SetFloat("Speed", Mathf.Abs(move));
		anim.SetBool("Grounded", grounded );

		rigidbody2D.velocity = new Vector2 (move * maxSpeed, rigidbody2D.velocity.y);



	}
}
